﻿using System;


namespace AutonomousCSharpScripter
{
    public interface ICodeSourceStorage
    {
        string  ScriptName                                { get; }
        int     TotalBytesAvailableForCode                { get; }
        bool    WrittingSourceCodeStartNewProgramInstance { get; }
        string  SourceCode                                { get; set; }
    }
}
