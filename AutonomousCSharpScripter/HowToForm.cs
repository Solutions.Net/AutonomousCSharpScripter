﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TechnicalTools.Tools;

namespace AutonomousCSharpScripter
{
    public partial class HowToForm : Form
    {
        public HowToForm()
        {
            InitializeComponent();
        }

        private void lnkAssociateFileExtension_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DefaultFileAssociations.SetAssociation(".c#s", "Autonomous_CSharp_Scripter", "C# Script");
        }
    }
}
