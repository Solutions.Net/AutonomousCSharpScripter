﻿using System;


namespace AutonomousCSharpScripter
{
    partial class ScriptManager
    {
        public static string DefaultCode
        {
            get
            {
                return (@"
using System; 
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Scripting
{
    class Program : AutonomousCSharpScripter.ScriptHelper
    {
        /// <summary>
        /// The main entry point for your script
        /// </summary>
        static void Main(string[] args)
        {
            //MessageBox.Show(" + "\"Hello world\"" + @");
            var frm = BuildPromptForm();
            var textAccess = frm.AddTextField(""some text"");
            var boolAccess = frm.AddBooleanField(""some bool"");
            var text2Access = frm.AddOpenFileField(""some text2"");
            frm.AddButtons(MessageBoxButtons.AbortRetryIgnore, 
            	dlgResult => MessageBox.Show(dlgResult + "" a été cliqué!"" + Environment.NewLine 
                           + textAccess.Item1() + "" "" + boolAccess.Item1() + "" "" + text2Access.Item1()));
                frm.ShowDialog();
            }
    }
}
").TrimStart();
            }
        }
    }
}
