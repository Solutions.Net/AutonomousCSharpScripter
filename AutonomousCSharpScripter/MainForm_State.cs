﻿using System;
using System.Windows.Forms;


namespace AutonomousCSharpScripter
{
    // Allow MainForm to store its UX states when program is restarted
    public partial class CommandLineSwitches
    {
        public FormWindowState?  WindowState { get; set; }
        public int?              Left        { get; set; }
        public int?              Top         { get; set; }
        public int?              Width       { get; set; }
        public int?              Height      { get; set; }
        public int?              SplitterPos { get; set; }
        public int?              SelStart    { get; set; }
        public int?              SelLength   { get; set; }
        public double?           VOffset     { get; set; }
        public double?           HOffset     { get; set; }
    }
}
