﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;


namespace AutonomousCSharpScripter
{
    public abstract class ScriptCompilerBase
    {
        public bool EnableDebugging { get; }

        protected ScriptCompilerBase()
        {
#if DEBUG
            EnableDebugging = true;
#else
            EnableDebugging = false;
#endif
        }
        public abstract Func<string[], int> BuildCodeAsMainMethod(Dictionary<string, string> sourceCodes);

        protected Func<string[], int> MainAssemblyToLambda(Assembly assembly)
        {
            // It is "almost" impossible to have more than one "Main" method bvecause compiler complains...
            // in visual studio we can have more than one Main method if we tell compielr with /main switch which method to use
            // Howver we dont provide a way to pass this ability to user of this current class so...
            // we can consider there is only one Main method, in all script, named "Main"
            var mMain = assembly.GetTypes().Select(t => t.GetMethod("Main", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static))
                                           .Single(m => m != null);
            return args =>
            {
                if (mMain.ReturnType == typeof(int))
                    if (mMain.GetParameters().Length == 0)
                        return (int)mMain.Invoke(null, null);
                    else
                        return (int)mMain.Invoke(null, new object[] { args });
                try
                {
                    mMain.Invoke(null, mMain.GetParameters().Length == 0 ? null : new object[] { args });
                    return 0;
                }
                catch (Exception ex)
                {
                    return ex.HResult;
                }
            };
        }
    }
}