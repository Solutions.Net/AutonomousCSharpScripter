﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Metadata;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;



namespace AutonomousCSharpScripter
{
    // with help of https://www.codeproject.com/Articles/1215168/Using-Roslyn-for-Compiling-Code-into-Separate-Net
    // https://dev.to/saka_pon/build-your-own-debugger-using-roslyns-syntax-analysis-1p44
    // https://stackoverflow.com/questions/50649795/how-to-debug-dll-generated-from-roslyn-compilation
    // https://developpaper.com/roslyn-embeds-the-source-code-into-the-pdb-symbol-file-through-embedallsources-to-facilitate-developer-debugging/
    public class ScriptCompilerWithRoslyn : ScriptCompilerBase
    {
        public class ScriptCompilationException : Exception
        {
            public IReadOnlyCollection<Diagnostic> Diagnostics { get; }

            public ScriptCompilationException(string message, IReadOnlyCollection<Diagnostic> results)
                : base(message)
            {
                Diagnostics = results;
            }
        }

        public override Func<string[], int> BuildCodeAsMainMethod(Dictionary<string, string> sourceCodes)
        {
            // code for class A
            //var classAString =
            //    @"public class A 
            //    {
            //        public static string Print() 
            //        { 
            //            return ""Hello "";
            //        }
            //    }";

            //// code for class B (to spice it up, it is a 
            //// subclass of A even though it is almost not needed
            //// for the demonstration)
            //var classBString =
            //    @"public class B : A
            //    {
            //        public static string Print()
            //        { 
            //            return ""World!"";
            //        }
            //    }";

            // the main class Program contain static void Main() 
            // that calls A.Print() and B.Print() methods
            var mainProgramString = string.Join(Environment.NewLine, sourceCodes.Select(kvp => "#line 1 \"" + kvp.Key + "\"" + Environment.NewLine + kvp.Value));

            //#region class A compilation into A.netmodule
            //// create Roslyn compilation for class A
            //var compilationA =
            //    CreateCompilationWithMscorlib
            //    (
            //        "A",
            //        classAString,
            //        compilerOptions: new CSharpCompilationOptions(OutputKind.NetModule)
            //    );

            //// emit the compilation result to a byte array 
            //// corresponding to A.netmodule byte code
            //byte[] compilationAResult = EmitToArray(compilationA);

            //// create a reference to A.netmodule
            //MetadataReference referenceA =
            //    ModuleMetadata
            //        .CreateFromImage(compilationAResult)
            //        .GetReference(display: "A.netmodule");
            //#endregion class A compilation into A.netmodule

            //#region class B compilation into B.netmodule
            //// create Roslyn compilation for class A
            //var compilationB =
            //    CreateCompilationWithMscorlib
            //    (
            //        "B",
            //        classBString,
            //        compilerOptions: new CSharpCompilationOptions(OutputKind.NetModule),

            //        // since class B extends A, we need to 
            //        // add a reference to A.netmodule
            //        references: new[] { referenceA }
            //    );

            //// emit the compilation result to a byte array 
            //// corresponding to B.netmodule byte code
            //byte[] compilationBResult = EmitToArray(compilationB);

            //// create a reference to B.netmodule
            //MetadataReference referenceB =
            //    ModuleMetadata
            //        .CreateFromImage(compilationBResult)
            //        .GetReference(display: "B.netmodule");
            //#endregion class B compilation into B.netmodule

            #region main program compilation into the assembly
            // create the Roslyn compilation for the main program with
            // ConsoleApplication compilation options
            // adding references to A.netmodule and B.netmodule
            var mainCompilation =
                CreateCompilationWithMscorlib
                (
                    "program",
                    mainProgramString,
                    compilerOptions: new CSharpCompilationOptions(OutputKind.ConsoleApplication)
                    //,references: new[] { referenceA, referenceB }
                );

            // Emit the byte result of the compilation
            byte[] result = EmitToArray(mainCompilation);

            // Load the resulting assembly into the domain. 
            Assembly assembly = Assembly.Load(result);
            #endregion main program compilation into the assembly

            // load the A.netmodule and B.netmodule into the assembly.
            //assembly.LoadModule("A.netmodule", compilationAResult);
            //assembly.LoadModule("B.netmodule", compilationBResult);

            #region Test the program
            // here we get the Program type and 
            // call its static method Main()
            // to test the program. 
            // It should write "Hello world!"
            // to the console

            return MainAssemblyToLambda(assembly);
            #endregion Test the program
        }
        
        // a utility method that creates Roslyn compilation for the passed code. 
        // The compilation references the collection of  passed "references" arguments plus
        // the mscore library (which is required for the basic functionality).
        static CSharpCompilation CreateCompilationWithMscorlib(string assemblyOrModuleName, string code, 
                                                               CSharpCompilationOptions compilerOptions = null, 
                                                               IEnumerable<MetadataReference> references = null)
        {
            // create the syntax tree
            SyntaxTree syntaxTree = SyntaxFactory.ParseSyntaxTree(code, new CSharpParseOptions(LanguageVersion.CSharp7_3, kind: SourceCodeKind.Regular), "generated.cs");

            var typeDependencies = new HashSet<Type>()
            {
                typeof(System.Int32), // mscorlib.dll
                typeof(System.ComponentModel.ArrayConverter), // System.dll
                typeof(System.Linq.Enumerable), // System.Core.dll
                typeof(System.Data.DataColumn),
                typeof(System.Xml.XmlDocument),
                typeof(System.Xml.Linq.XDocument),
                typeof(System.Text.RegularExpressions.Regex),
                typeof(System.Windows.Forms.Form),
                typeof(System.Web.HtmlString), // System.Web
                typeof(System.IO.Compression.ZipArchive), // System.IO.Compression
                typeof(System.IO.Compression.ZipFile), // System.IO.Compression.FileSystem
                typeof(System.Xml.XPath.Extensions), // Allow to get extension for xpath on Linq to Xml
                typeof(Microsoft.VisualBasic.FileIO.FileSystem), // Allow to move file to bin
                typeof(Program),
            };
            var assembliesToRefer = typeDependencies.Select(t => t.Assembly).Distinct().ToArray();
            var assembliesToReferLocations = assembliesToRefer
                                                .Select(ass => /*Path.GetFileName(*/ass.Location/*)*/)
                                                .Distinct()
                                                .ToArray();
            var allReferences = assembliesToReferLocations.Select(aLoc => AssemblyMetadata.CreateFromFile(aLoc).GetReference()).Cast<MetadataReference>();

            if (references != null)
                allReferences = allReferences.Concat(references); // no distinct ? it works ?

            // create and return the compilation
            CSharpCompilation compilation = CSharpCompilation.Create
            (
                assemblyOrModuleName,
                new[] { syntaxTree },
                options: compilerOptions,
                references: allReferences
            );

            return compilation;
        }
        
        // emit the compilation result into a byte array.
        // throw an exception with corresponding message if there are errors
        static byte[] EmitToArray(Compilation compilation)
        {
            using (var stream = new MemoryStream())
            {
                // emit result into a stream
                var emitResult = compilation.Emit(stream);

                if (!emitResult.Success)
                {
                    string errMsg = string.Join(Environment.NewLine,
                                                emitResult.Diagnostics.Where(d => d.Severity == DiagnosticSeverity.Error || d.Severity == DiagnosticSeverity.Warning && d.IsWarningAsError)
                                                                      .Select(error => $"Error {error.Id} (Line {error.Location.GetMappedLineSpan().StartLinePosition.Line}, Column {error.Location.GetMappedLineSpan().StartLinePosition.Character}) : {error.Descriptor}"));
                    if (errMsg.Length > 0)
                    {
                        errMsg += "Error compiling script !" + 
                                  Environment.NewLine + string.Join(Environment.NewLine,
                                  emitResult.Diagnostics.Where(d => d.Severity == DiagnosticSeverity.Warning && !d.IsWarningAsError)
                                                        .Select(error => $"Warning {error.Id} (Line {error.Location.GetMappedLineSpan().StartLinePosition.Line}, Column {error.Location.GetMappedLineSpan().StartLinePosition.Character}) : {error.Descriptor}"));
                        throw new ScriptCompilationException(errMsg, emitResult.Diagnostics);
                    }
                }

                // get the byte array from a stream
                return stream.ToArray();
            }
        }
    }
}