﻿using System;
using System.IO;

using System.Windows.Forms;


namespace AutonomousCSharpScripter
{
    public partial class CommandLineSwitches
    {
        public bool? NoGui { get; set; }
        public bool? Run   { get; set; }

        public string[] GetScriptArguments()
        {
            // original args minus all arguments in this partial class
            return Remove(Remove(OriginalArgs, nameof(NoGui)), nameof(Run));
        }
    }

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static int Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            try
            {
                var switches = new CommandLineSwitches(args);
                if (switches.Files.Count > 1)
                    throw new Exception("Only one file is allowed on command line (maybe you forgot \"--\" for some argument ?)");

                if (switches.Files.Count == 0)
                {
                    var t = typeof(Program);
                    using (Stream stream = t.Assembly.GetManifestResourceStream(t.Namespace + ".EmbeddedCode.txt"))
                    {
                        var replacer = new EmbeddedFileHotReplacer(stream, "\r\n|¤~-BEGIN-~¤|\r\n", "\r\n|¤~-END-~¤|\r\n", switches);
                        return Run(replacer, switches);
                    }
                }
                else if (switches.Files[0].EndsWith(".c#s"))
                {
                    var externalFile = new ExternalFile(switches.Files[0]);
                    return Run(externalFile, switches);
                }
                else
                    throw new Exception($"Unrecognized argument on command line {switches.Files[0]} (maybe you forgot \"--\" for some argument ?)");
            }
            catch (Exception ex)
            {
                DumpExceptionOnErrorStream(ex);
                return 1;
            }
        }

        static int Run(ICodeSourceStorage codeSource, CommandLineSwitches switches)
        {
            if ((switches.NoGui ?? false) || (switches.Run ?? false))
            {
                var executer = new ScriptManager();
                var result = executer.Compile(codeSource.SourceCode);
                if (result.scriptMain == null)
                {
                    Console.Error.WriteLine("** Script cannot be executed because it does not compile!");
                    foreach (var error in result.errors)
                        Console.Error.WriteLine(error);
                    foreach (var warning in result.warnings)
                        Console.Error.WriteLine(warning);
                    return -254;
                }
                try
                {
                    return executer.Run(result.scriptMain, switches.GetScriptArguments());
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("*** Script threw an excption:");
                    DumpExceptionOnErrorStream(ex);
                    return -255;
                }
            }
            Application.Run(new MainForm(switches, codeSource));
            return 0;
        }

        static void DumpExceptionOnErrorStream(Exception exception)
        {
            var ex = exception;
            while (ex != null)
            {
                if (ex != exception)
                    Console.Error.WriteLine("Because:");
                Console.Error.WriteLine("Unexpected error:" + Environment.NewLine + ex.ToString());
                ex = ex.InnerException;
            }
        }
    }
}
