﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using Microsoft.CodeAnalysis;
using Microsoft.CSharp;


namespace AutonomousCSharpScripter
{
    public class ScriptCompilerWithCodeDom : ScriptCompilerBase
    {
        public class ScriptCompilationException : Exception
        {
            public CompilerResults CompilerResults { get; }

            public ScriptCompilationException(string message, CompilerResults results)
                : base(message)
            {
                CompilerResults = results;
            }
        }

        public override Func<string[], int> BuildCodeAsMainMethod(Dictionary<string, string> sourceCodes)
        {
            // v4.0 is the max allowed version (for framework 4.5)
            // to use above version we have to use nuget Microsoft.CodeDom.Providers.DotNetCompilerPlatform.CSharpCodeProvider
            // which is heavy (a lot of tiny file with a global size of ~18Mo)
            var options = new Dictionary<string, string> { { "CompilerVersion", "v4.0" } };
            var provider = new CSharpCodeProvider();
            var parameters = new CompilerParameters
            {
                GenerateExecutable = true, // True - exe file generation, false - dll file generation
                TreatWarningsAsErrors = false,
                // Ces trois lignes de code permettent de voir la ligne qui a buggé en live dans Visual Studio!
                GenerateInMemory = !EnableDebugging,
                TempFiles = new TempFileCollection(Environment.GetEnvironmentVariable("TEMP"), true),
                IncludeDebugInformation = EnableDebugging,
            };

            // namespace System is present in assemblies mscorlib.dll, System.dll and System.Core.dll
            var typeDependencies = new HashSet<Type>()
            {
                typeof(System.Int32), // mscorlib.dll
                typeof(System.ComponentModel.ArrayConverter), // System.dll
                typeof(System.Linq.Enumerable), // System.Core.dll
                typeof(System.Data.DataColumn),
                typeof(System.Xml.XmlDocument),
                typeof(System.Xml.Linq.XDocument),
                typeof(System.Text.RegularExpressions.Regex),
                typeof(System.Windows.Forms.Form),
                typeof(System.Web.HtmlString), // System.Web
                typeof(System.IO.Compression.ZipArchive), // System.IO.Compression
                typeof(System.IO.Compression.ZipFile), // System.IO.Compression.FileSystem
                typeof(System.Xml.XPath.Extensions), // Allow to get extension for xpath on Linq to Xml
                typeof(Microsoft.VisualBasic.FileIO.FileSystem), // Allow to move file to bin
                typeof(Program),
            };
            var assembliesToRefer = typeDependencies.Select(t => t.Assembly).Distinct().ToArray();
            var assembliesToReferLocations = assembliesToRefer
                                                .Select(ass => Path.GetFileName(ass.Location))
                                                .Distinct()
                                                .ToArray();
            parameters.ReferencedAssemblies.AddRange(assembliesToReferLocations);


            CompilerResults results = provider.CompileAssemblyFromSource(parameters, sourceCodes.Select(kvp => "#line 1 \"" + kvp.Key + "\"" + Environment.NewLine + kvp.Value).ToArray());

            if (results.Errors.HasErrors)
            {
                string errMsg = "Error compiling script !" + Environment.NewLine +
                                string.Join(Environment.NewLine, results.Errors.Cast<CompilerError>().Where(error => !error.IsWarning).Select(error => $"Error {error.ErrorNumber} (Line {error.Line}, Column {error.Column}) : {error.ErrorText}")) +
                                Environment.NewLine +
                                string.Join(Environment.NewLine, results.Errors.Cast<CompilerError>().Where(error => error.IsWarning).Select(error => $"Warning {error.ErrorNumber} (Line {error.Line}, Column {error.Column}) : {error.ErrorText}"));
                throw new ScriptCompilationException(errMsg, results);
            }

            Assembly assembly = results.CompiledAssembly;
            return MainAssemblyToLambda(assembly);
        }
    }
}