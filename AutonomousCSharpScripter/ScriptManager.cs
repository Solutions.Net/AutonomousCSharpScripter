﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using TechnicalTools;


namespace AutonomousCSharpScripter
{
    partial class ScriptManager
    {
        public Task<(Func<string[], int> scriptMain, List<string> errors, List<string> warnings)> CompileAsync(string sourceCode, string filename = null, CancellationToken cancelToken = default(CancellationToken))
        {
            return Task.Run(() => Compile(sourceCode, filename, cancelToken));
        }

        /// <summary>
        /// Return a lambda with same signature than usual Main method.
        /// </summary>
        public (Func<string[], int> scriptMain, List<string> errors, List<string> warnings) Compile(string sourceCode, string filename = null, CancellationToken cancelToken = default(CancellationToken))
        {
            Dictionary<string, string> fullCodes = CompleteCode(sourceCode, filename ?? "<Embedded script>");

            var errors = new List<string>();
            var warnings = new List<string>();
            Func<string[], int> scriptMain = null;
            try
            {
                ScriptCompilerBase compiler = 
                    //new ScriptCompilerWithCodeDom();
                    new ScriptCompilerWithRoslyn();
                scriptMain = compiler.BuildCodeAsMainMethod(fullCodes);
            }
            catch (ScriptCompilerWithRoslyn.ScriptCompilationException cex)
            {
                foreach (var d in cex.Diagnostics)
                    if (d.Severity == Microsoft.CodeAnalysis.DiagnosticSeverity.Error || d.Severity == Microsoft.CodeAnalysis.DiagnosticSeverity.Warning && d.IsWarningAsError)
                    {
                        var line = d.ToString();
                        errors.Add(line);
                    }

                foreach (var d in cex.Diagnostics)
                    if (d.Severity == Microsoft.CodeAnalysis.DiagnosticSeverity.Warning && !d.IsWarningAsError)
                    {
                        var line = d.ToString();
                        warnings.Add(line);
                    }
            }
            catch (ScriptCompilerWithCodeDom.ScriptCompilationException cex)
            {
                foreach (var err in cex.CompilerResults.Errors.Cast<CompilerError>())
                    if (!err.IsWarning)
                    {
                        var line = (fullCodes.Count == 1 ? "" : Path.GetFileName(err.FileName) + " | ") 
                                 + $"Error {err.ErrorNumber} (Line {err.Line}, Column {err.Column}) : {err.ErrorText}";
                        errors.Add(line);
                    }

                foreach (var err in cex.CompilerResults.Errors.Cast<CompilerError>())
                    if (err.IsWarning)
                    {
                        var line = (fullCodes.Count == 1 ? "" : Path.GetFileName(err.FileName) + " | ")
                                 + $"Warning {err.ErrorNumber} (Line {err.Line}, Column {err.Column}) : {err.ErrorText}";
                        warnings.Add(line);
                    }
            }
            return (scriptMain, errors, warnings);
        }

        public Task<int> RunAsync(Func<string[], int> lastCompilation, string[] args, CancellationToken cancelToken = default(CancellationToken))
        {
            return Task.Run(() => Run(lastCompilation, args, cancelToken));
        }
        public int Run(Func<string[], int> work, string[] args, CancellationToken cancelToken = default(CancellationToken))
        {
            int? result = null;
            Exception exception = null;
            var thread = new Thread(_ =>
            {
                try
                {
                    result = work(args);
                }
                catch (ThreadAbortException ex) when (cancelToken.IsCancellationRequested)
                {
                    exception = ex;
                    Thread.ResetAbort();
                }
                catch (Exception ex)
                {
                    exception = ex;
                }
                finally
                {
                    Console.Out.Flush();
                    Console.Error.Flush();
                }
            });
            thread.Start();

            cancelToken.Register(() =>
            {
                // TODO : Killing/Aborting thread is very bad practice but we d'ont have choice here... Find another/proper way ?
                //        Check for example if adding a CanellationToken argument to script method is a good idea (as an overload)
                // TODO : Otherwise: Check that ressource are not locked forever, for example mutext
                // TODO :            Is the assembly compiled still in memory after execution and after user had release the labmda function ?
                thread.Abort();
            });

            thread.Join();

            if (exception is ThreadAbortException || exception is ThreadInterruptedException)
                throw new OperationCanceledException("User cancelled the script!", exception, cancelToken);
            if (exception != null)
                throw exception.Rethrow();
            return result.Value;
        }


        /// <summary>Just take script and handle #include directive to get the complete/full code</summary>
        /// <param name="sourceCode">source code</param>
        /// <param name="scriptName">script file path (if code is from a file) or any other name</param>
        /// <returns>Piece of code to compile in the returned order, script file path (or name) => content (without include)</returns>
        Dictionary<string, string> CompleteCode(string sourceCode, string scriptName)
        {
            if (sourceCode == null)
                return null;

            var filesContent = new Dictionary<string, Tuple<Include, string>>();
            var filesToAnalyse = new Queue<Include>();

            var root = new Include()
            {
                IsLibrary = false,
                ScriptName = scriptName,
            };
            filesToAnalyse.Enqueue(root);
            filesContent.Add(Path.GetFullPath(root.ScriptName), Tuple.Create(root, sourceCode));

            while (filesToAnalyse.Count > 0)
            {
                var include = filesToAnalyse.Dequeue();
                var path = Path.GetFullPath(include == root ? include.ScriptName : include.FilePath);

                var content = include == root ? sourceCode : File.ReadAllText(path);
                var subIncludes = ExtractIncludes(Path.GetFileName(path), content, include.IsLibrary).ToArray();
                foreach (var subInclude in subIncludes)
                {
                    var fp = Path.GetFullPath(subInclude.FilePath);
                    if (!filesContent.ContainsKey(fp))
                    {
                        filesToAnalyse.Enqueue(subInclude);
                        filesContent.Add(fp, Tuple.Create(subInclude, (string)null)); // to detect recursive loop
                    }
                    else if (filesContent[fp].Item1.IsLibrary != subInclude.IsLibrary)
                        throw new Exception($"File \"{Path.GetFileName(fp)}\" is simultaneously considered both a standard/non standard library in files " +
                                            $"\"{filesContent[fp].Item1.ScriptName}\" and \"{subInclude.ScriptName}!");
                }

                // Remove includes
                foreach (var subInclude in subIncludes.Reverse())
                    content = content.Substring(0, subInclude.Index) + content.Substring(subInclude.Index + subInclude.Length);
                filesContent[path] = Tuple.Create(filesContent[path].Item1, content);
            }
            return filesContent.Values.OrderBy(tuple => tuple.Item1.ScriptName == root.ScriptName ? 1 : 0)
                                      .OrderBy(tuple => tuple.Item1.IsLibrary ? 0 : 1)
                                      .ThenBy(tuple => tuple.Item1.FilePath ?? scriptName)
                                      .ToDictionary(tuple => tuple.Item1.FilePath ?? scriptName, tuple => tuple.Item2);
        }

        IEnumerable<Include> ExtractIncludes(string scriptName, string codeSource, bool isLibrary)
        {
            int lineIndex = 1;
            bool lineBeginWithSpaces = true;
            bool onlyStandardInclude = true;
            int lineColumnStart = 0;
            int i = -1;
            bool tooLate = false;
            Func<string, string> error = msg => $"Script: {scriptName}, Line {lineIndex}, Column {i - lineColumnStart}: " + msg;
            while (++i < codeSource.Length)
            {
                char c = codeSource[i];
                if (c == '\n')
                {
                    ++lineIndex;
                    lineBeginWithSpaces = true;
                    lineColumnStart = i;
                }
                else if (c == '{') // first namespace or class
                {
                    tooLate = true;
                }
                else if (lineBeginWithSpaces)
                {
                    lineBeginWithSpaces &= char.IsWhiteSpace(c);
                    if (c == '#' && i + 8 < codeSource.Length &&
                        string.Equals(codeSource.Substring(i, 8), "#include", StringComparison.OrdinalIgnoreCase) &&
                        char.IsWhiteSpace(codeSource[i + 8]))
                    {
                        if (tooLate)
                            throw new Exception(error("Unexpected #include here. Include must before the first namespace of class in the file"));

                        var startInclude = i;
                        // Skip Spaces
                        i += 9;
                        while (i <= codeSource.Length && char.IsWhiteSpace(codeSource[i]) && codeSource[i] != '\r' && codeSource[i] != '\n')
                            ++i;
                        if (i >= codeSource.Length)
                            throw new Exception(error("Unexpected end of file"));
                        if (codeSource[i] == '\r' && codeSource[i] == '\n')
                            throw new Exception(error("Unexpected end of line"));

                        // Detect opening of filename in include
                        char openFileChar = codeSource[i];
                        if (openFileChar == '<')
                        {
                            if (!onlyStandardInclude)
                                throw new Exception(error("Standard includes (using <...>) must always be before local includes (using \"...\")"));
                        }
                        else if (openFileChar != '"')
                            throw new Exception(error($"Unrecognised character '{openFileChar}'. Expecting '\"' or '<'"));
                        else if (isLibrary)
                            throw new Exception(error($"Character '<' must be used because this script file {scriptName} has been included as standard library"));

                        // parse filename in include
                        ++i;
                        var startFile = i;
                        while (i < codeSource.Length && codeSource[i] != '>' && codeSource[i] != '"' && codeSource[i] != '\r' && codeSource[i] != '\n')
                            ++i;
                        if (i >= codeSource.Length)
                            throw new Exception(error("Unexpected end of file"));
                        if (codeSource[i] == '\r' && codeSource[i] == '\n')
                            throw new Exception(error("Unexpected end of line"));
                        char closingFileChar = codeSource[i];
                        if (openFileChar == '<' && closingFileChar == '"')
                            throw new Exception(error($"Expecting character '>' here, not '{closingFileChar}'"));
                        if (openFileChar == '"' && closingFileChar == '>')
                            throw new Exception(error($"Expecting character '\"' here, not '{closingFileChar}'"));
                        
                        var filepath = codeSource.Substring(startFile, i - startFile);
                        ++i;
                        while (i <= codeSource.Length && char.IsWhiteSpace(codeSource[i]) && codeSource[i] != '\r' && codeSource[i] != '\n')
                            ++i;
                        if (i <= codeSource.Length && codeSource[i] != '\r' && codeSource[i] != '\n')
                            throw new Exception(error($"Unexpected character {codeSource[i]}, expecting end of line"));
                        yield return new Include()
                        {
                            FilePath = filepath,
                            IsLibrary = openFileChar == '<',
                            ScriptName = scriptName,
                            // Index and length for all #include line
                            Index = startInclude,
                            Length = i - startInclude, // do not take final \r \n, it can be useful to keep newline when replacing include later
                            Line = lineIndex,
                        };
                    }
                }
            }
        }
        class Include
        {
            public string FilePath   { get; set; }
            public bool   IsLibrary  { get; set; }
            public string ScriptName { get; set; }
            public int    Index      { get; set; } // index of beginning of line, not '"'
            public int    Length     { get; set; } // does not include final \r or \n
            public int    Line       { get; set; } // starting from 1
        }
    }
}
