using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

using ICSharpCode.AvalonEdit.Folding;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;

using TechnicalTools;
using TechnicalTools.Tools;
using TechnicalTools.UI;

using DefaultControls.Gui.Tools;


namespace AutonomousCSharpScripter
{
    public partial class MainForm : Form
    {
        public MainForm(CommandLineSwitches switches = null, ICodeSourceStorage sourceStorage = null)
        {
            _sourceStorage = sourceStorage;
            _switches = switches ?? new CommandLineSwitches(new string[0]);
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            _scriptManager = new ScriptManager();

            Text = Path.GetFileNameWithoutExtension(Application.ExecutablePath).Uncamelify()
                 + " - Autonomous C# Scripter";
            tslVersion.Text = " v2021.05.14";
            _txtCode = new ICSharpCode.AvalonEdit.TextEditor();
            wpfHoster.Child = _txtCode;
            ConfigureAvalonEditor();

            ResumeFromRestart(_switches);
            Resize += (_, __) => RelocateCodeSizeLimitLabel();
        }
        readonly CommandLineSwitches _switches;
        readonly ICodeSourceStorage _sourceStorage;
        readonly ICSharpCode.AvalonEdit.TextEditor _txtCode;
        readonly ScriptManager _scriptManager;

        string              _lastCodeSaved;
        bool                CodeHasChanges { get { return _txtCode.Text != _lastCodeSaved; } }

        private void MainForm_Load(object sender, EventArgs e)
        {
            LoadContent();
            TeeTextWriter.MirrorConsoleOutputTo(new MirrorToTextboxTextWriter(txtStdOutput));
            TeeTextWriter.MirrorConsoleErrorTo(new MirrorToTextboxTextWriter(txtStdErrors));
            RefreshEnabilitiesAndVisibilities();
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            _txtCode.TextArea.Caret.Offset = _txtCode.Text.Length;
            _txtCode.Focus();
            _txtCode.TextArea.Caret.BringCaretToView();
        }

        void RefreshEnabilitiesAndVisibilities()
        {
            btnSave.Enabled = CodeHasChanges;
            // We do not disable btnCompile here because user can change also library script that are included
        }
        #region Save /restart Form State (after writting SourceCode )

        void SaveState()
        {
            _switches.WindowState = WindowState;
            _switches.Left = Left;
            _switches.Top = Top;
            _switches.Width = Width;
            _switches.Height = Height;
            _switches.SplitterPos = splitContainer1.SplitterDistance;
            _switches.SelStart = _txtCode.SelectionStart;
            _switches.SelLength = _txtCode.SelectionLength;
            _switches.VOffset = _txtCode.VerticalOffset;
            _switches.HOffset = _txtCode.HorizontalOffset;
        }
        void ResumeFromRestart(CommandLineSwitches sw)
        {
            if (sw.WindowState == null)
                return;

            StartPosition = FormStartPosition.Manual;
            Bounds = new System.Drawing.Rectangle(sw.Left ?? Left, sw.Top ?? Top, sw.Width ?? Width, sw.Height ?? Height);
            WindowState = sw.WindowState ?? WindowState;

            splitContainer1.SplitterDistance = sw.SplitterPos ?? splitContainer1.SplitterDistance;

            Shown += (_, __) =>
            {
                if (sw.SelStart.HasValue)
                    _txtCode.SelectionStart = Math.Min(_txtCode.Text.Length, sw.SelStart.Value);
                if (sw.SelLength.HasValue)
                    _txtCode.SelectionLength = sw.SelLength.Value;
                if (sw.VOffset.HasValue)
                    _txtCode.ScrollToVerticalOffset(sw.VOffset.Value);
                if (sw.HOffset.HasValue)
                    _txtCode.ScrollToHorizontalOffset(sw.HOffset.Value);
                _txtCode.Focus();
            };
        }

        #endregion


        void ConfigureAvalonEditor()
        {
            _txtCode.ShowLineNumbers = true;
            _txtCode.TextArea.Caret.PositionChanged += Caret_PositionChanged;
            _txtCode.TextArea.SelectionChanged  += Caret_PositionChanged;
            _txtCode.TextChanged += (_, __) => RelocateCodeSizeLimitLabel();
            _txtCode.TextChanged += (_, __) => RefreshEnabilitiesAndVisibilities();

            // CONFIGURE HIGHLIGHT
            // Liste des resource existante (ensemble de regles pour la coloration) https://github.com/icsharpcode/AvalonEdit/tree/master/ICSharpCode.AvalonEdit/Highlighting/Resources
            // Pour programmer ca par le code : http://www.kellen.tech/questions/983772/adding-syntax-highlighting-rules-to-avalonedit-programmatically
            using (Stream s = _txtCode.GetType().Assembly.GetManifestResourceStream("ICSharpCode.AvalonEdit.Highlighting.Resources.CSharp-Mode.xshd"))
            {
                Debug.Assert(s != null, nameof(s) + " != null");
                using (XmlTextReader reader = new XmlTextReader(s))
                {
                    _txtCode.SyntaxHighlighting = HighlightingLoader.Load(reader, HighlightingManager.Instance);
                }
            }

            // CONFIGURE CODE FOLDING
            // officla doc: http://avalonedit.net/documentation/html/440df648-413e-4f42-a28b-6b2b0e9b1084.htm
            // to do folding manually : https://stackoverflow.com/questions/5246467/bracefolding-in-avalonedit
            _foldingManager = FoldingManager.Install(_txtCode.TextArea);
            _foldingStrategy = new XmlFoldingStrategy();
            UpdateCodeFolding();
        }
        FoldingManager _foldingManager;
        XmlFoldingStrategy _foldingStrategy;

        // To call regularly to update folding
        void UpdateCodeFolding()
        {
            _foldingStrategy.UpdateFoldings(_foldingManager, _txtCode.Document);
        }

        private void Caret_PositionChanged(object sender, EventArgs e)
        {
            const string space = "    ";
            tslPosition.Text = "Ln : " + _txtCode.TextArea.Caret.Line
                             + space
                             + "Col : " + _txtCode.TextArea.Caret.Column
                             + space
                             + "Sel : " + _txtCode.TextArea.Selection.Length + " | " + Math.Abs(_txtCode.TextArea.Selection.EndPosition.Line - _txtCode.TextArea.Selection.StartPosition.Line);
        }

        void RelocateCodeSizeLimitLabel()
        {
            rightDistance = rightDistance ?? lblCodeSizeLimit.Parent.Width - lblCodeSizeLimit.Right;
            _lblCodeSizeLimitTextPrefix = _lblCodeSizeLimitTextPrefix ?? lblCodeSizeLimit.Text;
            lblCodeSizeLimit.Text = _lblCodeSizeLimitTextPrefix + _txtCode.Text.Length.ToString("n0") + " / " + _sourceStorage.TotalBytesAvailableForCode.ToString("n0");
            lblCodeSizeLimit.Left = lblCodeSizeLimit.Parent.Width - lblCodeSizeLimit.Width - rightDistance.Value;
            Debug.Assert(rightDistance.Value == lblCodeSizeLimit.Parent.Width - lblCodeSizeLimit.Right);
        }
        int? rightDistance;
        string _lblCodeSizeLimitTextPrefix;

        void LoadContent()
        {
            try
            {
                var content = _sourceStorage.SourceCode;
                if (string.IsNullOrWhiteSpace(content))
                    content = ScriptManager.DefaultCode;
                _lastCodeSaved = content;
                _txtCode.Text = content;
                UpdateCodeFolding();
                // If txtCode is a regular TextBox
                //txtCode.SelectionStart = txtCode.Text.Length;
                //txtCode.DeselectAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (CodeHasChanges)
            {
                e.Cancel = DialogResult.Cancel 
                        == MessageBox.Show(this, "Leave without saving ?", "Please confirm", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (_sourceStorage.WrittingSourceCodeStartNewProgramInstance)
                    SaveState();
                _sourceStorage.SourceCode = _txtCode.Text;
                _lastCodeSaved = _txtCode.Text; // always needed otherwise the Close call below will warn user (and block) about unsaved changes
                if (_sourceStorage.WrittingSourceCodeStartNewProgramInstance)
                {
                    Thread.Sleep(500);
                    Close();
                }
                RefreshEnabilitiesAndVisibilities();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async void btnCompile_Click(object sender, EventArgs e)
        {
            _lastCompilation = null;
            _lastCompilation = await Compile();
        }
        async Task<Func<string[], int>> Compile(CancellationToken cancelToken = default(CancellationToken))
        {
            var code = _txtCode.Text; // No trim here to preserve line reference in error
            txtCompilationErrors.Clear();

            // do not check for CodeHasChanges because user may want to compile again
            // after having changed an included script in another program
            Func<string[], int> scriptMain;
            List<string> errors;
            List<string> warnings;
            try
            {
                (scriptMain, errors, warnings) = await _scriptManager.CompileAsync(code, _sourceStorage.ScriptName, cancelToken);
                foreach (var msg in errors.Concat(warnings))
                    txtCompilationErrors.AppendText(msg + Environment.NewLine);
                if (errors.Concat(warnings).Any())
                    tabs.SelectedTab = tabCompilation;
                if (scriptMain != null)
                    txtCompilationErrors.AppendText("Compilation has succeeded." + Environment.NewLine);
                return scriptMain;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Unexpected exception happened!" + Environment.NewLine + ex.ToString());
                return null;
            }
        }
        Func<string[], int> _lastCompilation;
        
        private async void btnRun_Click(object sender, EventArgs e)
        {
            if (_currentExecutionCancelToken != null)
            {
                _currentExecutionCancelToken.Cancel(); //should unlock quickly the await below
                return;
            }

            if (CodeHasChanges || _lastCompilation == null)
            {
                _lastCompilation = await Compile();
                if (_lastCompilation == null)
                    return;
            }
            
            _currentExecutionCancelToken = new CancellationTokenSource();
            try
            {
                await Run(_lastCompilation, _currentExecutionCancelToken.Token);
            }
            finally
            {
                _currentExecutionCancelToken.Dispose();
                _currentExecutionCancelToken = null;
            }
        }
        CancellationTokenSource _currentExecutionCancelToken;

        async Task<int?> Run(Func<string[], int> lastCompilation, CancellationToken cancelToken = default(CancellationToken))
        {
            // Clear UI
            txtStdOutput.Clear();
            txtStdErrors.Clear();
            tabs.SelectedTab = tabStdOutput;
            
            // Disable UI for User
            wpfHoster.Enabled = false;
            btnSave.Enabled = false;
            btnCompile.Enabled = false;
            btnRun.Tag = btnRun.Tag ?? btnRun.Text; // keep string defined at design time
            btnRun.Text = "Stop / Kill";
            try
            {
                var result = await _scriptManager.RunAsync(lastCompilation, new string[0], cancelToken);

                AddFinalLine(txtStdOutput, "[IDE] : Script's result code is: " + result);
                return result;
            }
            catch (OperationCanceledException)
            {
                AddFinalLine(txtStdErrors, "[IDE] : Script's execution has been aborted!");
            }
            catch (Exception ex)
            {
                AddFinalLine(txtStdErrors, "[IDE] : Script raised an exception: " + Environment.NewLine + ex.ToString());
            }
            finally
            {
                // Enable Back UI for user
                wpfHoster.Enabled = true;
                btnSave.Enabled = true;
                btnCompile.Enabled = true;
                btnRun.Text = (string)btnRun.Tag;
            }
            return null;
        }

        void AddFinalLine(TextBox txtBox, string msg)
        {
            var text = txtBox.Text;
            if (text.Length > 0 && text.Last() != '\n')
                txtBox.AppendText(Environment.NewLine);
            txtBox.AppendText(msg + Environment.NewLine);
        }
        

        private void tsHowTo_Click(object sender, EventArgs e)
        {
            var frm = new HowToForm();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog(this);
        }

        private void tsAbout_Click(object sender, EventArgs e)
        {
            Process.Start("https://gitlab.com/Solutions.Net/AutonomousCSharpScripter");
        }
    }
}
