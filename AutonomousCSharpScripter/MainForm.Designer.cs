﻿namespace AutonomousCSharpScripter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCompile = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lblCodeSizeLimit = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.wpfHoster = new System.Windows.Forms.Integration.ElementHost();
            this.lblTitleMessage = new System.Windows.Forms.Label();
            this.tabs = new System.Windows.Forms.TabControl();
            this.tabCompilation = new System.Windows.Forms.TabPage();
            this.txtCompilationErrors = new System.Windows.Forms.TextBox();
            this.tabStdOutput = new System.Windows.Forms.TabPage();
            this.txtStdOutput = new System.Windows.Forms.TextBox();
            this.tabStdErrors = new System.Windows.Forms.TabPage();
            this.txtStdErrors = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsHowTo = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslSpaceFiller = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsAbout = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslSpaceFiller2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslPosition = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabs.SuspendLayout();
            this.tabCompilation.SuspendLayout();
            this.tabStdOutput.SuspendLayout();
            this.tabStdErrors.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 288);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(767, 43);
            this.panel1.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel3.Controls.Add(this.btnSave);
            this.panel3.Controls.Add(this.btnCompile);
            this.panel3.Controls.Add(this.btnRun);
            this.panel3.Location = new System.Drawing.Point(116, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(535, 31);
            this.panel3.TabIndex = 5;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(3, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(115, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save script";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCompile
            // 
            this.btnCompile.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnCompile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompile.Location = new System.Drawing.Point(232, 4);
            this.btnCompile.Name = "btnCompile";
            this.btnCompile.Size = new System.Drawing.Size(86, 23);
            this.btnCompile.TabIndex = 3;
            this.btnCompile.Text = "Compile";
            this.btnCompile.Click += new System.EventHandler(this.btnCompile_Click);
            // 
            // btnRun
            // 
            this.btnRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(417, 4);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(115, 23);
            this.btnRun.TabIndex = 4;
            this.btnRun.Text = "Run";
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lblCodeSizeLimit);
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            this.splitContainer1.Panel1.Controls.Add(this.lblTitleMessage);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabs);
            this.splitContainer1.Size = new System.Drawing.Size(767, 572);
            this.splitContainer1.SplitterDistance = 331;
            this.splitContainer1.TabIndex = 5;
            // 
            // lblCodeSizeLimit
            // 
            this.lblCodeSizeLimit.AutoSize = true;
            this.lblCodeSizeLimit.Location = new System.Drawing.Point(670, 5);
            this.lblCodeSizeLimit.Name = "lblCodeSizeLimit";
            this.lblCodeSizeLimit.Size = new System.Drawing.Size(98, 13);
            this.lblCodeSizeLimit.TabIndex = 9;
            this.lblCodeSizeLimit.Text = "Allowed code size: ";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.wpfHoster);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 24);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(767, 264);
            this.panel2.TabIndex = 8;
            // 
            // wpfHoster
            // 
            this.wpfHoster.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wpfHoster.Font = new System.Drawing.Font("Consolas", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wpfHoster.Location = new System.Drawing.Point(0, 0);
            this.wpfHoster.Name = "wpfHoster";
            this.wpfHoster.Size = new System.Drawing.Size(765, 262);
            this.wpfHoster.TabIndex = 1;
            this.wpfHoster.Text = "elementHost1";
            this.wpfHoster.Child = null;
            // 
            // lblTitleMessage
            // 
            this.lblTitleMessage.AutoSize = true;
            this.lblTitleMessage.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitleMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitleMessage.Location = new System.Drawing.Point(0, 0);
            this.lblTitleMessage.Name = "lblTitleMessage";
            this.lblTitleMessage.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.lblTitleMessage.Size = new System.Drawing.Size(215, 24);
            this.lblTitleMessage.TabIndex = 5;
            this.lblTitleMessage.Text = "Edit your C# script and run it !";
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.tabCompilation);
            this.tabs.Controls.Add(this.tabStdOutput);
            this.tabs.Controls.Add(this.tabStdErrors);
            this.tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabs.Location = new System.Drawing.Point(0, 0);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(767, 237);
            this.tabs.TabIndex = 6;
            // 
            // tabCompilation
            // 
            this.tabCompilation.Controls.Add(this.txtCompilationErrors);
            this.tabCompilation.Location = new System.Drawing.Point(4, 22);
            this.tabCompilation.Name = "tabCompilation";
            this.tabCompilation.Padding = new System.Windows.Forms.Padding(3);
            this.tabCompilation.Size = new System.Drawing.Size(759, 211);
            this.tabCompilation.TabIndex = 0;
            this.tabCompilation.Text = "Compilation Errors";
            this.tabCompilation.UseVisualStyleBackColor = true;
            // 
            // txtCompilationErrors
            // 
            this.txtCompilationErrors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCompilationErrors.Location = new System.Drawing.Point(3, 3);
            this.txtCompilationErrors.Multiline = true;
            this.txtCompilationErrors.Name = "txtCompilationErrors";
            this.txtCompilationErrors.Size = new System.Drawing.Size(753, 205);
            this.txtCompilationErrors.TabIndex = 6;
            // 
            // tabStdOutput
            // 
            this.tabStdOutput.Controls.Add(this.txtStdOutput);
            this.tabStdOutput.Location = new System.Drawing.Point(4, 22);
            this.tabStdOutput.Name = "tabStdOutput";
            this.tabStdOutput.Padding = new System.Windows.Forms.Padding(3);
            this.tabStdOutput.Size = new System.Drawing.Size(759, 212);
            this.tabStdOutput.TabIndex = 1;
            this.tabStdOutput.Text = "Std Output";
            this.tabStdOutput.UseVisualStyleBackColor = true;
            // 
            // txtStdOutput
            // 
            this.txtStdOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtStdOutput.Location = new System.Drawing.Point(3, 3);
            this.txtStdOutput.Multiline = true;
            this.txtStdOutput.Name = "txtStdOutput";
            this.txtStdOutput.Size = new System.Drawing.Size(753, 206);
            this.txtStdOutput.TabIndex = 7;
            // 
            // tabStdErrors
            // 
            this.tabStdErrors.Controls.Add(this.txtStdErrors);
            this.tabStdErrors.Location = new System.Drawing.Point(4, 22);
            this.tabStdErrors.Name = "tabStdErrors";
            this.tabStdErrors.Padding = new System.Windows.Forms.Padding(3);
            this.tabStdErrors.Size = new System.Drawing.Size(759, 212);
            this.tabStdErrors.TabIndex = 2;
            this.tabStdErrors.Text = "Std Errors";
            this.tabStdErrors.UseVisualStyleBackColor = true;
            // 
            // txtStdErrors
            // 
            this.txtStdErrors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtStdErrors.Location = new System.Drawing.Point(3, 3);
            this.txtStdErrors.Multiline = true;
            this.txtStdErrors.Name = "txtStdErrors";
            this.txtStdErrors.Size = new System.Drawing.Size(753, 206);
            this.txtStdErrors.TabIndex = 8;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsAbout,
            this.tslVersion,
            this.tslSpaceFiller,
            this.tsHowTo,
            this.tslSpaceFiller2,
            this.tslPosition});
            this.statusStrip1.Location = new System.Drawing.Point(0, 572);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(767, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsHowTo
            // 
            this.tsHowTo.IsLink = true;
            this.tsHowTo.Name = "tsHowTo";
            this.tsHowTo.Size = new System.Drawing.Size(54, 17);
            this.tsHowTo.Text = "How to ?";
            this.tsHowTo.Click += new System.EventHandler(this.tsHowTo_Click);
            // 
            // tslSpaceFiller
            // 
            this.tslSpaceFiller.Name = "tslSpaceFiller";
            this.tslSpaceFiller.Size = new System.Drawing.Size(230, 17);
            this.tslSpaceFiller.Spring = true;
            // 
            // tslVersion
            // 
            this.tslVersion.Name = "tslVersion";
            this.tslVersion.Size = new System.Drawing.Size(61, 17);
            this.tslVersion.Text = "<version>";
            // 
            // tsAbout
            // 
            this.tsAbout.IsLink = true;
            this.tsAbout.Name = "tsAbout";
            this.tsAbout.Size = new System.Drawing.Size(40, 17);
            this.tsAbout.Text = "About";
            this.tsAbout.Click += new System.EventHandler(this.tsAbout_Click);
            // 
            // tslSpaceFiller2
            // 
            this.tslSpaceFiller2.Name = "tslSpaceFiller2";
            this.tslSpaceFiller2.Size = new System.Drawing.Size(230, 17);
            this.tslSpaceFiller2.Spring = true;
            // 
            // tslPosition
            // 
            this.tslPosition.Name = "tslPosition";
            this.tslPosition.Size = new System.Drawing.Size(137, 17);
            this.tslPosition.Text = "Ln : 0    Col : 0    Sel : 0 | 0";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 594);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Autonomous C# script runner";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabs.ResumeLayout(false);
            this.tabCompilation.ResumeLayout(false);
            this.tabCompilation.PerformLayout();
            this.tabStdOutput.ResumeLayout(false);
            this.tabStdOutput.PerformLayout();
            this.tabStdErrors.ResumeLayout(false);
            this.tabStdErrors.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lblTitleMessage;
        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage tabCompilation;
        private System.Windows.Forms.TextBox txtCompilationErrors;
        private System.Windows.Forms.TabPage tabStdOutput;
        private System.Windows.Forms.TextBox txtStdOutput;
        private System.Windows.Forms.TabPage tabStdErrors;
        private System.Windows.Forms.TextBox txtStdErrors;
        private System.Windows.Forms.Integration.ElementHost wpfHoster;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnCompile;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslSpaceFiller;
        private System.Windows.Forms.ToolStripStatusLabel tslPosition;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ToolStripStatusLabel tslVersion;
        private System.Windows.Forms.Label lblCodeSizeLimit;
        private System.Windows.Forms.ToolStripStatusLabel tsAbout;
        private System.Windows.Forms.ToolStripStatusLabel tsHowTo;
        private System.Windows.Forms.ToolStripStatusLabel tslSpaceFiller2;
    }
}

