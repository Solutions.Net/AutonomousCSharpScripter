﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using TechnicalTools;


namespace AutonomousCSharpScripter
{
    public partial class CommandLineSwitches
    {
        public string[] OriginalArgs { get; }
        public Dictionary<string, string> UnparsedArgs { get; }
        public List<string> Files { get; }

        public CommandLineSwitches(string[] args)
        {
            OriginalArgs = args;
            UnparsedArgs = new Dictionary<string, string>();
            Files = new List<string>();
            var props = GetInterestingProperties().ToDictionary(p => p.Name.ToLowerInvariant());
            foreach (var arg in args)
            {
                if (arg.StartsWith("--"))
                {
                    var index = arg.IndexOf('=');
                    if (index > 0)
                    {
                        var argName = arg.Substring(2, index - 2);
                        var argValue = arg.Substring(index + 1);
                        PropertyInfo pi;
                        if (props.TryGetValue(argName.ToLowerInvariant(), out pi))
                        {
                            var value = ParseCommandLineArgValue(pi, argName, argValue);
                            pi.SetValue(this, value);
                        }
                        else
                            UnparsedArgs[argName.ToLowerInvariant()] = argValue;
                    }
                    else
                    {
                        var argName = arg.Substring(2);
                        PropertyInfo pi;
                        if (props.TryGetValue(argName.ToLowerInvariant(), out pi))
                            if (pi.PropertyType == typeof(bool) || pi.PropertyType == typeof(bool?))
                                pi.SetValue(this, true);
                            else
                                throw new Exception($"Missing value for argument \"{argName}\"!");
                        else
                            UnparsedArgs[argName.ToLowerInvariant()] = null;
                    }
                }
                else
                    Files.Add(arg);
            }
        }
        IEnumerable<PropertyInfo> GetInterestingProperties()
        {
            var props = GetType().GetProperties()
                                 .Where(p => p.CanWrite);
            return props;
        }
        public bool Has(string lowerArgName)
        {
            return UnparsedArgs.ContainsKey(lowerArgName);
        }


        public string[] Remove(string[] args, string argName)
        {
            return args.Where(a => a.ToLowerInvariant() != "--" + argName.ToLowerInvariant()
                                && !a.ToLowerInvariant().StartsWith("--" + argName.ToLowerInvariant() + "="))
                       .ToArray();
        }

        public string[] BuildCommandLine(params string[] propNames)
        {
            if (propNames == null)
                return BuildCommandLine();
            var allProps = GetInterestingProperties().ToDictionary(p => p.Name.ToLowerInvariant());
            var props = propNames.Select(pn => allProps[pn.ToLowerInvariant()]).ToList();
            return BuildCommandLine(props);
        }
        public string[] BuildCommandLine(List<PropertyInfo> props = null)
        {
            props = props ?? GetInterestingProperties().ToList();
            var result = new string[props.Count];
            int i = 0;
            foreach (var pi in props)
            {
                if (pi.GetValue(this) == null)
                    continue;

                result[i] = "--" + pi.Name;
                if (pi.PropertyType.RemoveNullability() == typeof(bool) && (bool)pi.GetValue(this))
                {
                    // skip "=true"
                }
                else
                {
                    result[i] += "=";
                    result[i] += ConvertToNotEpuratedCommandLineArgValue(pi);
                }
                ++i;
            }
            if (i == props.Count)
                return result;
            return result.Take(i).ToArray();
        }


        // If you change this method, change also ConvertToNotEpuratedCommandLineArgValue
        object ParseCommandLineArgValue(PropertyInfo pi, string argName, string epuratedArgValue)
        {
            if (pi.PropertyType == typeof(string))
                return epuratedArgValue;
            else if (pi.PropertyType.RemoveNullability() == typeof(bool))
                return epuratedArgValue.ToLowerInvariant() == "true" || epuratedArgValue == "1";
            else if (pi.PropertyType.RemoveNullability() == typeof(int))
            {
                if (!int.TryParse(epuratedArgValue, NumberStyles.Integer, CultureInfo.InvariantCulture, out int v))
                    throw new Exception($"Argument \"{argName}\" has invalid value \"{epuratedArgValue}\"");
                return v;
            }
            else if (pi.PropertyType.RemoveNullability() == typeof(double))
            {
                // we do not use NumberStyles.Float because it contains AllowTrailingSign
                var styles = NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint 
                           | NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite;
                if (!double.TryParse(epuratedArgValue, styles, CultureInfo.InvariantCulture, out double v))
                    throw new Exception($"Argument \"{argName}\" has invalid value \"{epuratedArgValue}\"");
                return v;
            }
            else if (pi.PropertyType.RemoveNullability().IsEnum)
            {
                Enum v;
                try
                {
                    v = (Enum)Enum.Parse(pi.PropertyType.RemoveNullability(), epuratedArgValue, true);
                }
                catch (Exception ex)
                {
                    throw new Exception($"Argument \"{argName}\" has invalid value \"{epuratedArgValue}\"", ex);
                }
                return v;
            }
            else
                throw new Exception($"Do not know how to treat argument \"{argName}\"!");
        }

        // If you change this method, change also ParseCommandLineArgValue
        object ConvertToNotEpuratedCommandLineArgValue(PropertyInfo pi)
        {
            if (pi.PropertyType == typeof(string))
                // The converted value is not epurated here because we have to handle console escapement
                return "\"" + string.Join(" ", (string)pi.GetValue(this)).Replace("\"", "\"\"") + "\"";
            else if (pi.PropertyType.RemoveNullability() == typeof(bool))
                return (bool)pi.GetValue(this) ? "true" : "false";
            else if (pi.PropertyType.RemoveNullability() == typeof(int))
                return ((int)pi.GetValue(this)).ToString(CultureInfo.InvariantCulture);
            else if (pi.PropertyType.RemoveNullability() == typeof(double))
                return ((double)pi.GetValue(this)).ToString(CultureInfo.InvariantCulture);
            else if (pi.PropertyType.RemoveNullability().IsEnum)
                return ((Enum)pi.GetValue(this)).ToString();
            else
                throw new Exception($"Do not know how to treat property \"{pi.Name}\"!");
        }


        // From https://stackoverflow.com/questions/298830/split-string-containing-command-line-parameters-into-string-in-c-sharp
        public string[] CommandLineToArgs(string commandLine)
        {
            int argc;
            var argv = CommandLineToArgvW(commandLine, out argc);
            if (argv == IntPtr.Zero)
                throw new System.ComponentModel.Win32Exception();
            try
            {
                var args = new string[argc];
                for (var i = 0; i < args.Length; i++)
                {
                    var p = Marshal.ReadIntPtr(argv, i * IntPtr.Size);
                    args[i] = Marshal.PtrToStringUni(p);
                }

                return args;
            }
            finally
            {
                Marshal.FreeHGlobal(argv);
            }
        }

#pragma warning disable CS0809 // ignore obsolete
        [Obsolete("Reserved to debugging! Make code explicit, use " + nameof(CommandLineToArgs) + "!")]
        public override string ToString()
        {
            return base.ToString();
        }
#pragma warning restore CS0809

        [DllImport("shell32.dll", SetLastError = true)]
        static extern IntPtr CommandLineToArgvW([MarshalAs(UnmanagedType.LPWStr)] string lpCmdLine, out int pNumArgs);
    }
}
