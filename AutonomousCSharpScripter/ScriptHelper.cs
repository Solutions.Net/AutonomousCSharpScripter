﻿using System;
using System.Linq;
using System.Windows.Forms;


namespace AutonomousCSharpScripter
{
    public class ScriptHelper
    {

        /// <summary>
        /// Example 
        /// <code>
        /// var frm = BuildPromptForm();
        /// var textAccess = frm.AddTextField("some text");
        /// var boolAccess = frm.AddBooleanField("some bool");
        /// var fileAccess = frm.AddOpenFileField("file");
        /// var folderAccess = frm.AddOpenFolderField("folder");
        /// var saveFileAccess = frm.AddSaveFileField("file");
        /// var saveFolderAccess = frm.AddSaveFolderField("folder");
        /// frm.AddButtons(MessageBoxButtons.AbortRetryIgnore,
        /// dlgResult => MessageBox.Show(dlgResult + " a été cliqué!" + Environment.NewLine
        ///                + textAccess.Item1() + " " + boolAccess.Item1() + " " + text2Access.Item1()));
        ///     frm.ShowDialog();
        /// </code>
        /// </summary>
        public static PromptForm BuildPromptForm()
        {
            return new PromptForm();
        }

        public class PromptForm : Form
        {
            public PromptForm()
            {
                StartPosition = FormStartPosition.CenterScreen;
                Padding = new Padding(3, 0, 3, 0);
            }

            public (Func<string> get, Action<string> set) AddTextField(string caption)
            {
                var txt = new TextBox();
                txt.Dock = DockStyle.Fill;
                txt.Location = new System.Drawing.Point(35, 0);
                txt.Name = "txt" + _fieldCount.ToString(System.Globalization.CultureInfo.InvariantCulture);
                BuildFieldPanel(caption, txt);
                return (() => txt.Text, text => txt.Text = text);
            }
            public (Func<bool> get, Action<bool> set) AddBooleanField(string caption)
            {
                var chk = new CheckBox();
                chk.Dock = DockStyle.Fill;
                chk.Location = new System.Drawing.Point(35, 0);
                chk.Name = "chk" + _fieldCount.ToString(System.Globalization.CultureInfo.InvariantCulture);
                BuildFieldPanel(caption, chk);
                return (() => chk.Checked, value => chk.Checked = value);
            }

            public (Func<string> get, Action<string> set) AddOpenFileField(string caption)
            {
                var txt = new TextBox();
                txt.Dock = DockStyle.Fill;
                txt.Location = new System.Drawing.Point(35, 0);
                txt.Name = "txt" + _fieldCount.ToString(System.Globalization.CultureInfo.InvariantCulture);
                var panel = BuildFieldPanel(caption, txt);
                var btnBrowse = new Button();
                btnBrowse.Text = "...";
                btnBrowse.Size = new System.Drawing.Size(28, 22);
                btnBrowse.Dock = DockStyle.Right;
                panel.Controls.Add(btnBrowse);
                return (() => txt.Text, text => txt.Text = text);
            }
            //(Func<string> get, Action<string> set) AddOpeneFileField()
            //{
                // var fileAccess = frm.AddOpenFileField("file");
                // var folderAccess = frm.AddOpenFolderField("folder");
                // var saveFileAccess = frm.AddSaveFileField("file");
                // var saveFolderAccess = frm.AddSaveFolderField("folder");
            //}



            public void AddButtons(MessageBoxButtons buttons, Action<DialogResult> callback)
            {
                var btnOk = buttons == MessageBoxButtons.OK || buttons == MessageBoxButtons.OKCancel ? new Button() { Text = "Ok" } : null;
                if (btnOk != null) btnOk.Click += (_, __) => DialogResult = DialogResult.OK;
                var btnAbort = buttons == MessageBoxButtons.AbortRetryIgnore ? new Button() { Text = "Abort" } : null;
                if (btnAbort != null) btnAbort.Click += (_, __) => DialogResult = DialogResult.Abort; 
                var btnRetry = buttons == MessageBoxButtons.AbortRetryIgnore || buttons == MessageBoxButtons.RetryCancel ? new Button() { Text = "Retry" } : null;
                if (btnRetry != null) btnRetry.Click += (_, __) => DialogResult = DialogResult.Retry; 
                var btnIgnore = buttons == MessageBoxButtons.AbortRetryIgnore ? new Button() { Text = "Ignore" } : null;
                if (btnIgnore != null) btnIgnore.Click += (_, __) => DialogResult = DialogResult.Ignore; 
                var btnYes = buttons == MessageBoxButtons.YesNoCancel || buttons == MessageBoxButtons.YesNo ? new Button() { Text = "Yes" } : null;
                if (btnYes != null) btnYes.Click += (_, __) => DialogResult = DialogResult.Yes; 
                var btnNo = buttons == MessageBoxButtons.YesNoCancel || buttons == MessageBoxButtons.YesNo ? new Button() { Text = "No" } : null;
                if (btnNo != null) btnNo.Click += (_, __) => DialogResult = DialogResult.No; 
                var btnCancel = buttons == MessageBoxButtons.OKCancel || buttons == MessageBoxButtons.YesNoCancel || buttons == MessageBoxButtons.RetryCancel ? new Button() { Text = "Cancel" } : null;
                if (btnCancel != null) btnCancel.Click += (_, __) => DialogResult = DialogResult.Cancel;

                var btns = new[] { btnOk, btnAbort, btnRetry, btnIgnore, btnYes, btnNo, btnCancel }.Where(b => b != null).ToArray();
                var pnl = StackPanelFromTopToBottom();
                pnl.Height = 25;
                pnl.Width = ClientSize.Width;
                SuspendLayout();
                bool buttonClicked = false;
                for (int i = 0; i < btns.Length; ++i)
                {
                    var btn = btns[i];
                    btn.Name = "btn" + _fieldCount.ToString(System.Globalization.CultureInfo.InvariantCulture) + btn.Text;
                    btn.Click += (_, __) => { buttonClicked = true; callback(DialogResult); };
                    pnl.Controls.Add(btn);
                }
                void repositionButtons()
                {
                    for (int i = 0; i < btns.Length; ++i)
                    {
                        var btn = btns[i];
                        var left = i * pnl.Width / btns.Length;
                        btn.Bounds = new System.Drawing.Rectangle(left, 0, (i + 1) * pnl.Width / btns.Length - left, pnl.Height);
                    }
                }
                repositionButtons();
                pnl.Resize += (_, __) => repositionButtons();
                pnl.Dock = DockStyle.Bottom; // don't know why but it makes  pnl.Width >> ClientSize.Width!
                FormClosed += (_, __) => { if (!buttonClicked) callback(DialogResult.Cancel); };
                ResumeLayout();
            }

            Panel BuildFieldPanel(string caption, Control ctl)
            {
                var pnl = StackPanelFromTopToBottom();
                var lbl = new Label();
                lbl.AutoSize = true;
                lbl.Dock = DockStyle.Left;
                lbl.Location = new System.Drawing.Point(0, 0);
                lbl.Name = "lbl" + _fieldCount.ToString(System.Globalization.CultureInfo.InvariantCulture);
                lbl.Padding = new Padding(0, 4, 0, 0);
                lbl.Size = new System.Drawing.Size(35, 17);
                lbl.Text = caption;

                pnl.Padding = new Padding(3, 0, 3, 0);
                pnl.Controls.Add(ctl);
                pnl.Controls.Add(lbl);
                pnl.Tag = caption;
                pnl.BringToFront();
                SuspendLayout();
                var w = lbl.Size.Width;
                if (w > _maxWidth)
                {
                    _maxWidth = w;
                    foreach (Panel otherPnl in Controls)
                    {
                        var otherLbl = (Label)otherPnl.Controls[otherPnl.Controls.Count - 1];
                        var h = lbl.Size.Height;
                        otherLbl.AutoSize = false;
                        otherLbl.Width = _maxWidth;
                        otherLbl.Height = h;
                    }
                }
                ResumeLayout();
                return pnl;
            }
            Panel StackPanelFromTopToBottom()
            {
                ++_fieldCount;
                var pnl = new Panel();
                pnl.Dock = DockStyle.Top;
                pnl.Location = new System.Drawing.Point(3, 4);
                pnl.Name = "pnl" + _fieldCount.ToString(System.Globalization.CultureInfo.InvariantCulture);
                pnl.Size = new System.Drawing.Size(438, 25);
                Controls.Add(pnl);
                return pnl;
            }
            int _fieldCount;
            int _maxWidth;
        }
    }
}
