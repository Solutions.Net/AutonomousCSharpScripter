﻿using System;
using System.Diagnostics;
using System.Runtime.ExceptionServices;


namespace TechnicalTools
{
    public static partial class Exception_Extensions
    {
        /// <summary>
        /// This method just rethrow an exception without reseting stacktrace !
        /// User should call throw on the result. But this throw won't be executed.
        /// It just help compiler to analyse execution flow of your code and stick with what you really want to do
        /// rather than forcing to initialize things and write reutn statement if throw keyword was not used.
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        public static Exception Rethrow(this Exception ex)
        {
            ExceptionDispatchInfo.Capture(ex).Throw();
            Debug.Fail("Unreachable code reached");
            return ex;
        }
    }
}
