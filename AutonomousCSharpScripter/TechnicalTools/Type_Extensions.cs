﻿using System;


namespace TechnicalTools
{
    public static class Type_Extensions
    {
        public static Type RemoveNullability(this Type type)
        {
            return TryGetNullableType(type) ?? type;
        }
        static Type TryGetNullableType(this Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                return type.GetGenericArguments()[0];
            return null;
        }
    }
}
