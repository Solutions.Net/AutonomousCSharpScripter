﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;


namespace DefaultControls.Gui.Tools
{
    public class MirrorToTextboxTextWriter : TextWriter
    {
        readonly StringBuilder _sb = new StringBuilder(1024);
        readonly TextBox _txt;
      

        public MirrorToTextboxTextWriter(TextBox txt)
        {
            _txt = txt;
        }

        public override Encoding Encoding
        {
            get { return Encoding.Unicode; }
        }
        
        public override void Flush()
        {
            base.Flush();
            lock (_sb)
            {
                string str = _sb.ToString();
                _sb.Clear();

                if (_txt.InvokeRequired || _hasBeenInvoked)
                {
                    _txt.BeginInvoke((Action)(() => _txt.AppendText(str)));
                    _hasBeenInvoked = true; // remember so that we can preserve the sequentiality of line of text
                }
                else
                    _txt.AppendText(str);
            }
        }
        bool _hasBeenInvoked;

        public override void Write(char value)
        {            
            lock (_sb)
            {
                _sb.Append(value);
                if (value == '\n')
                    Flush();
            }
        }
    }
}
