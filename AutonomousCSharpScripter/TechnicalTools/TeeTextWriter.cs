﻿using System;
using System.IO;
using System.Text;


namespace TechnicalTools.Tools
{
    public class TeeTextWriter : TextWriter
    {
        public static TeeTextWriter MirrorConsoleOutputTo(TextWriter t)
        {
            var oldOne = Console.Out;
            var res = new TeeTextWriter(oldOne, t, preventDisposingFirstOnClose: true);
            res.Disposed += (_, __) => Console.SetOut(oldOne);
            Console.SetOut(res);
            return res;
        }
        public static TeeTextWriter MirrorConsoleErrorTo(TextWriter t)
        {
            var oldOne = Console.Error;
            var res = new TeeTextWriter(oldOne, t, preventDisposingFirstOnClose: true);
            res.Disposed += (_, __) => Console.SetError(oldOne);
            Console.SetError(res);
            return res;
        }

        public TeeTextWriter(TextWriter one, TextWriter two, bool preventDisposingFirstOnClose = false)
        {
            _preventDisposingFirstOnClose = preventDisposingFirstOnClose;
            _one = one;
            _two = two;
        }
        readonly TextWriter _one;
        readonly TextWriter _two;
        readonly bool _preventDisposingFirstOnClose;

        public override Encoding Encoding
        {
            get { return _one.Encoding; }
        }

        public override void Flush()
        {
            _one.Flush();
            _two.Flush();
        }

        // The base method is mark as virtual but should be abstract
        // All the other write method redirect to the implementation of this one
        public override void Write(char value)
        {
            _one.Write(value);
            _two.Write(value);
        }

        public event EventHandler Disposed;

        protected override void Dispose(bool disposing)
        {
            if (!_preventDisposingFirstOnClose)
                _one.Dispose();
            _two.Dispose();
            Disposed?.Invoke(this, EventArgs.Empty);
        }
    }
}
