﻿using System;
using System.Text;


namespace TechnicalTools
{
    public static class String_Extensions
    {
        /// <summary> Insère un espace avant chaque lettre majuscule (sauf si plusieurs lettres majuscules se suivent... dans ce cas on garde le bloc de lettre majuscule) </summary>
        public static string Uncamelify(this string str)
        {
            var sb = new StringBuilder();
            bool lastCapital = true;
            foreach (char c in str)
            {
                var isCapital = char.IsUpper(c);
                if (isCapital && !lastCapital)
                    sb.Append(' ');
                sb.Append(c);
                lastCapital = isCapital;
            }
            return sb.ToString();
        }
    }
}
