﻿using System;
using System.Text;


namespace TechnicalTools.Extensions
{
    public static class ByteArray_Extensions
    {
        public static byte[] SubArray(this byte[] bytes, int index) { return SubArray(bytes, index, unchecked(bytes.Length - index)); }
        public static byte[] SubArray(this byte[] bytes, int index, int length)
        {
            if (bytes == null)
                throw new ArgumentNullException(nameof(bytes), "Value cannot be null");
            if (index < 0 || index >= bytes.Length)
                throw new ArgumentOutOfRangeException(nameof(bytes), "index is out of range");
            if (index + length > bytes.Length)
                throw new ArgumentOutOfRangeException(nameof(bytes), "length is out of range");
            var res = new byte[length];
            Buffer.BlockCopy(bytes, index, res, 0, length);
            return res;
        }

        public static int IndexOf(this byte[] bytes, string str, Encoding encoding = null)
        {
            if (str == null)
                throw new ArgumentNullException(nameof(str), "Value cannot be null");
            var needle = (encoding ?? Encoding.UTF8).GetBytes(str);
            return IndexOf(bytes, needle);
        }
        public static int IndexOf(this byte[] haystack, byte[] needle)
        {
            if (haystack == null)
                throw new ArgumentNullException(nameof(haystack), "Value cannot be null");
            if (needle == null)
                throw new ArgumentNullException(nameof(needle), "Value cannot be null");

            var len = needle.Length;
            if (len == 0)
                return 0;
            var limit = haystack.Length - len + 1;

            for (int h = 0; h < limit; ++h)
                if (haystack[h] == needle[0])
                {
                    int n = 0;
                    while (++n < len)
                        if (haystack[h + n] != needle[n])
                            break;
                    if (n == len)
                        return h;
                }
            return -1;
        }
    }
}