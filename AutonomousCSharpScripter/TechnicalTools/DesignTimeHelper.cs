﻿using System.ComponentModel;
using System.Diagnostics;


namespace TechnicalTools.UI
{
    public static class DesignTimeHelper
    {
        /// <summary>
        /// This method is very useful to know if code is executed in design mode.
        /// VS's property "DesignMode" does not work very well (at least for version before 2013). 
        /// <note>I never had problem with this custom implementation so I never checked again with more recent VS.</note>
        /// </summary>
        public static bool IsInDesignMode
        {
            [DebuggerHidden, DebuggerStepThrough]
            get
            {
                if (_isInDesignMode.HasValue)
                    return _isInDesignMode.Value;

                if (LicenseManager.UsageMode == LicenseUsageMode.Designtime)
                {
                    _isInDesignMode = true;
                    return true;
                }

                // UsageMode is wrong sometimes, so...
                try
                {
                    // Ugly but work very well. I never get a problem with this code.
                    using (var process = Process.GetCurrentProcess())
                        _isInDesignMode = process.ProcessName.ToLowerInvariant().Contains("devenv");
                }
                catch // It never happened yet :)
                {
                    // Can't know :( because of weird thing...
                    // We consider making release product working a priority
                    // So we use false value
                    if (Debugger.IsAttached) // Important ! Otherwise Break can make app crashes when built in release mode.
                        Debugger.Break();
                    _isInDesignMode = false;
                }
                return _isInDesignMode.Value;
            }
        }
        static bool? _isInDesignMode;
    }
}
