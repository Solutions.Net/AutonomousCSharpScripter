﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

using Microsoft.Win32;


namespace TechnicalTools.Tools
{
    // Thanks to https://stackoverflow.com/a/44816953
    public class DefaultFileAssociations
    {
        /// <summary>Install the mapping between extension and a default program, for the current user only</summary>
        /// <param name="extension">Extension, with leading dot included</param>
        /// <param name="progId">Name of application (space seems to be forbidden, to check...)</param>
        /// <param name="fileTypeDescription">File Extension Name, for example "XML File"</param>
        /// <param name="applicationFilePath">Full absolute path of executable, By default Process.GetCurrentProcess().MainModule.FileName is used</param>
        /// <returns></returns>
        public static bool SetAssociation(string extension, string progId, string fileTypeDescription, string applicationFilePath = null)
        {
            applicationFilePath = applicationFilePath ?? Process.GetCurrentProcess().MainModule.FileName;
            bool madeChanges = false;
            madeChanges |= SetKeyDefaultValue( @"Software\Classes\" + extension, progId);
            madeChanges |= SetKeyDefaultValue( @"Software\Classes\" + progId, fileTypeDescription);
            madeChanges |= SetKeyDefaultValue($@"Software\Classes\{progId}\shell\open\command", "\"" + applicationFilePath + "\" \"%1\"");
            SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_FLUSH, IntPtr.Zero, IntPtr.Zero);
            return madeChanges;
        }

        static bool SetKeyDefaultValue(string keyPath, string value)
        {
            // using CurrentUser allows to set values without needing to have admin priviledges
            using (var key = Registry.CurrentUser.CreateSubKey(keyPath))
                if (key.GetValue(null) as string != value)
                {
                    key.SetValue(null, value);
                    return true;
                }

            return false;
        }

        // needed so that Explorer windows get refreshed after the registry is updated
        [DllImport("Shell32.dll")]
        private static extern int SHChangeNotify(int eventId, int flags, IntPtr item1, IntPtr item2);

        private const int SHCNE_ASSOCCHANGED = 0x8000000;
        private const int SHCNF_FLUSH = 0x1000;
    }
}
