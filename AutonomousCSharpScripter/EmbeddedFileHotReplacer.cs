﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TechnicalTools.Extensions;


namespace AutonomousCSharpScripter
{
    public partial class CommandLineSwitches
    {
        public string Delete { get; set; }
    }

    // This class allow you to change embeded data (text) in current executable and recreate that executable
    // How to use it :
    // 1) Create an embbeded file in your executable
    // 2) The content must begins with a weird and rare prefix (for example "|¤~-BEGIN-~¤|")
    //    and end with another weird and rare suffix (for example "|¤~-END-~¤|")
    public class EmbeddedFileHotReplacer : ICodeSourceStorage
    {
        #region Implement ICodeSourceStorage

        public string   ScriptName                                { get { return "<Embedded script>"; } }
        public int      TotalBytesAvailableForCode                { get; }
        public bool     WrittingSourceCodeStartNewProgramInstance { get { return true; } }
        public string   SourceCode                                { get { return GetSourceCode(); } set { SetSourceCode(value); } }

        #endregion

        public EmbeddedFileHotReplacer(Stream resource, string prefix, string suffix, CommandLineSwitches state)
        {
            if (prefix == suffix)
                throw new ArgumentException("prefix and suffix cannot be the same !");
            _resource = resource;
            _prefix = prefix;
            _suffix = suffix;
            _prefixAsBytes = Encoding.UTF8.GetBytes(prefix);
            _suffixAsBytes = Encoding.UTF8.GetBytes(suffix);
            _state = state;
            TotalBytesAvailableForCode = (int)_resource.Length - _prefixAsBytes.Length - _suffixAsBytes.Length;
            ApplyCommandsFromPreviousProcessExecution();
        }
        readonly Stream _resource;
        readonly string _prefix; readonly byte[] _prefixAsBytes;
        readonly string _suffix; readonly byte[] _suffixAsBytes;
        readonly CommandLineSwitches _state;

        public string GetSourceCode()
        {
            _resource.Position = 0;
            int i = 0;
            var bs = new byte[_resource.Length];
            while (i < _resource.Length)
            {
                var read = _resource.Read(bs, i, (int)_resource.Length - i);
                if (read == 0)
                    throw new Exception("Cannot read resource!");
                i += read;
            }
            Debug.Assert(bs[0] != 239, "Remove BOM ! https://en.wikipedia.org/wiki/Byte_order_mark");
            Debug.Assert(bs.IndexOf(_prefixAsBytes) == 0);
            Debug.Assert(bs.IndexOf(_suffixAsBytes) == bs.Length - _suffixAsBytes.Length);
            var text = Encoding.UTF8.GetString(bs, _prefixAsBytes.Length, bs.Length - _prefixAsBytes.Length - _suffixAsBytes.Length);
            text = text.TrimEnd();
            return text;
        }

        public void SetSourceCode(string newContent)
        {
            string currentExePath = Application.ExecutablePath;
            Debug.Assert(Path.GetDirectoryName(currentExePath) != null);

            // Generate a temp file
            var tempNewExeFilename = CreateNewExecutableWithNewContent(currentExePath, newContent);
            Debug.Assert(Path.GetFileName(tempNewExeFilename) != null);
            
            // Move it next to the current exe if there is enough space (otherwise the file is not copied at all, even partially)
            var newExeFilenameBySide = Path.Combine(Path.GetDirectoryName(currentExePath), Path.GetFileName(tempNewExeFilename));

            MakeSureFileNameIsAvailableByRolling(newExeFilenameBySide);
            File.Move(tempNewExeFilename, newExeFilenameBySide);

            // rename the current exe
            var oldCurrentExePath = currentExePath + ".old";
            MakeSureFileNameIsAvailableByRolling(oldCurrentExePath);
            File.Move(currentExePath, oldCurrentExePath);

            // Assign the new file to current exe's name
            File.Move(newExeFilenameBySide, currentExePath);    

            _state.Delete = oldCurrentExePath; // will be recognised in ApplyCommandsFromPreviousProcessExecution

            var psi = new ProcessStartInfo
            {
                FileName = Application.ExecutablePath,
                WorkingDirectory = Path.GetDirectoryName(Application.ExecutablePath),
                Arguments = string.Join(" ", _state.BuildCommandLine())
            };
            Process.Start(psi);
        }

        void ApplyCommandsFromPreviousProcessExecution()
        {
            if (_state.Delete == null)
                return;
            System.Threading.Tasks.Task.Run(() =>
            {
                int remainingTries = 60;
                while (remainingTries > 0)
                {
                    try
                    {
                        // Note: if file already does not exist, it does not throw
                        File.Delete(_state.Delete);
                        break;
                    }
                    // Exception is very likely to be about delete right
                    catch
                    {
                        try
                        {
                            // Optimistically we try to rename the file so user can still save file as much as he want
                            MakeSureFileNameIsAvailableByRolling(_state.Delete, ".toDeleteManually");
                            _state.Delete += ".toDeleteManually";
                        }
                        catch
                        {
                            // Ignored :( 
                            // TODO : It never happened yet. but find a way to handle properly this case 
                            // for example through an event bus manager (we raise a warning and UI catch it) 
                        }
                        // But we continue to try to delete it (maybe it is locked)
                        if (--remainingTries > 0)
                        {
                            System.Threading.Thread.Sleep(1000);
                            continue;
                        }
                    }
                }
            });
        }

        // The filename of the file generated has no the same name of currentExePath
        string CreateNewExecutableWithNewContent(string currentExePath, string newContent)
        {
            var newExeBytes = GetBytesOfNewExeWithNewContent(currentExePath, newContent);

            var updatedExePath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(currentExePath) + ".exe.updated");
            MakeSureFileNameIsAvailableByRolling(updatedExePath);
            File.WriteAllBytes(updatedExePath, newExeBytes);
            return updatedExePath;
        }
        byte[] GetBytesOfNewExeWithNewContent(string currentExePath, string code)
        {
            code = code.TrimEnd();
            if (code.Length > 0)
             code += Environment.NewLine;

            var codePart = Encoding.UTF8.GetBytes(code);
            if (codePart.Length > TotalBytesAvailableForCode)
                throw new Exception("Sorry, the text to save is too long of approximatively " + (codePart.Length - TotalBytesAvailableForCode) + " characters");
            if (codePart.IndexOf(_prefixAsBytes) >= 0)
                throw new Exception($"Sorry, you cannot save the text \"{_prefix}\"!");
            if (codePart.IndexOf(_suffixAsBytes) >= 0)
                throw new Exception($"Sorry, you cannot save the text \"{_suffix}\"!");

            // We need to add padding in order to not change the structure of executable.
            var emptyLine = Environment.NewLine + new string(' ', 2048 - Environment.NewLine.Length);
            var sb = new StringBuilder();
            var tmp = (TotalBytesAvailableForCode - codePart.Length) % emptyLine.Length;
            sb.Append(new string(' ', tmp));
            for (int i = 0; i < (TotalBytesAvailableForCode - codePart.Length) / emptyLine.Length; ++i)
                sb.Append(emptyLine);
            var padding = Encoding.UTF8.GetBytes(sb.ToString());

            var bs = File.ReadAllBytes(currentExePath);
            var index = bs.IndexOf(_prefixAsBytes);
            Debug.Assert(index >= 0);
            var binaryBeforeContent = bs.SubArray(0, index + _prefixAsBytes.Length);

            var indexOfBinaryAfterContent = bs.IndexOf(_suffixAsBytes);
            Debug.Assert(indexOfBinaryAfterContent > index);
            var binaryAfterContent = bs.SubArray(indexOfBinaryAfterContent);


            var res = binaryBeforeContent
                .Concat(codePart)
                .Concat(padding)
                .Concat(binaryAfterContent)
                .ToArray();

            return res;
        }
        
        // Make sure tempExePath is a filename available by rolling any history
        void MakeSureFileNameIsAvailableByRolling(string filename, string ext = ".toDeleteManually")
        {
            string rolling = filename;
            int i = 0;
            while (File.Exists(rolling))
            {
                ++i;
                rolling = filename + ext + (i > 1 ? i.ToString(CultureInfo.InvariantCulture) : "");
            }
            while (rolling != filename && --i >= 0)
            {
                string tmp = filename + (i > 0 ? ext : "") + (i > 1 ? i.ToString(CultureInfo.InvariantCulture) : "");
                File.Move(tmp, rolling);
                rolling = tmp;
            }
            Debug.Assert(!File.Exists(filename));
        }
    }
}
