﻿
namespace AutonomousCSharpScripter
{
    partial class HowToForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HowToForm));
            this.btnClose = new System.Windows.Forms.Button();
            this.lblinfo = new System.Windows.Forms.Label();
            this.lnkAssociateFileExtension = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(594, 415);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // lblinfo
            // 
            this.lblinfo.AutoSize = true;
            this.lblinfo.Location = new System.Drawing.Point(41, 9);
            this.lblinfo.Name = "lblinfo";
            this.lblinfo.Size = new System.Drawing.Size(614, 416);
            this.lblinfo.TabIndex = 1;
            this.lblinfo.Text = resources.GetString("lblinfo.Text");
            // 
            // lnkAssociateFileExtension
            // 
            this.lnkAssociateFileExtension.AutoSize = true;
            this.lnkAssociateFileExtension.Location = new System.Drawing.Point(536, 204);
            this.lnkAssociateFileExtension.Name = "lnkAssociateFileExtension";
            this.lnkAssociateFileExtension.Size = new System.Drawing.Size(87, 13);
            this.lnkAssociateFileExtension.TabIndex = 2;
            this.lnkAssociateFileExtension.TabStop = true;
            this.lnkAssociateFileExtension.Text = "Associate \".c#s\"";
            this.lnkAssociateFileExtension.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkAssociateFileExtension_LinkClicked);
            // 
            // HowToForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(681, 450);
            this.Controls.Add(this.lnkAssociateFileExtension);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblinfo);
            this.Name = "HowToForm";
            this.Text = "How to ?";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblinfo;
        private System.Windows.Forms.LinkLabel lnkAssociateFileExtension;
    }
}