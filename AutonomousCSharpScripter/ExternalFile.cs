﻿using System;
using System.IO;


namespace AutonomousCSharpScripter
{
    // Simple wrapper class around an external file to edit it
    public class ExternalFile : ICodeSourceStorage
    {
        #region Implement ICodeSourceStorage

        public string   ScriptName                                { get { return Path.GetFileName(_filePath); } }
        public int      TotalBytesAvailableForCode                { get { return int.MaxValue; } }
        public bool     WrittingSourceCodeStartNewProgramInstance { get { return false; } }
        public string   SourceCode                                { get { return GetSourceCode(); } set { SetSourceCode(value); } }

        #endregion

        public ExternalFile(string filePath)
        {
            _filePath = filePath;
        }
        readonly string _filePath;

        string GetSourceCode()
        {
            try
            {
                return File.ReadAllText(_filePath);
            }
            catch (FileNotFoundException)
            {
                return "";
            }
        }
        void SetSourceCode(string sourceCode)
        {
            File.WriteAllText(_filePath, sourceCode);
        }
    }
}
